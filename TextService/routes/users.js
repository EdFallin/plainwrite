/**/

let express = require("express");

let router = express.Router();
let service = null;

// Bridge between ESM and CJS.
async function lazyInitService() {
  if (service !== null) {
    return;
  }

  let source = await import("../api/user-service.js");
  service = new source.UserService();
}

// For initial calls to set state of UX.
router.get("/condition", function (req, res, next) {
  res.send({ isLoggedIn: req.session.isLoggedIn || false });
});

// To log the user in if their credentials are valid.
router.post("/login", function (req, res, next) {
  let individual = req.body.individual;
  let secret = req.body.secret;

  lazyInitService()
    .then(() => service.doMatch(individual, secret))
    .then((didMatch) => {
      req.session.isLoggedIn = didMatch;
      res.send({ isLoggedIn: req.session.isLoggedIn });
    });
});

// To log the user out.
router.post("/logout", function (req, res, next) {
  req.session.isLoggedIn = false;
  res.send({ isLoggedIn: req.session.isLoggedIn });
});

// Required for app.js to use.
module.exports = router;

