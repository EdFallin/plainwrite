/**/

let express = require("express");

let router = express.Router();
let service = null;

// Bridge between ESM of my API and CJS of Express.
async function lazyInitService() {
  // Only initing once.
  if (service !== null) {
    return;
  }

  // Localizing the module, calling its constructor member.
  let source = await import("../api/text-service.js");
  service = new source.TextService();
}

/* &cruft, perhaps handle null returns either as 404s or something */

/* Getting all existing texts. */
router.get("/get", function (req, res, next) /* verified */ {
  // Sad path.  Not authorized.
  if (!req.session.isLoggedIn) {
    res.sendStatus(403);
    return;
  }

  // Main / happy path, authorized.
  // All texts found and returned.
  lazyInitService()
    .then(() => service.getAllTexts())
    .then((texts) => {
      if (texts === null) {
        throw new Error();
      }

      res.send(texts);
    })
    .catch(() => {
      // Generic server fail.
      res.sendStatus(500);
    });
});

/* Saving a new text with the existing ones. */
router.post("/post", function (req, res, next) {
  // Sad path.  Not authorized.
  if (!req.session.isLoggedIn) {
    res.sendStatus(403);
    return;
  }

  /* Main / happy path, authorized.
   * New text added, all texts returned. */

  let text = req.body.text;
  let datetime = req.body.datetime;

  lazyInitService()
    .then(() => service.saveNewText({ text, datetime }))
    .then((all) => {
      if (all === null) {
        throw new Error();
      }

      // Returning all to reduce round-trips.
      res.send(all);
    })
    .catch(() => {
      // Generic server fail.
      res.sendStatus(500);
    });
});

/* Getting all texts as one unparsed JSON string. */
router.get("/json", function (req, res, next) /* verified */ {
  // Sad path.  Not authorized.
  if (!req.session.isLoggedIn) {
    res.sendStatus(403);
    return;
  }

  /* Main / happy path, authorized.
   * All JSON in texts file returned. */

  lazyInitService()
    .then(() => service.getAllJson())
    .then((jsonText) => {
      if (jsonText === null) {
        throw new Error();
      }

      res.send(jsonText);
    })
    .catch(() => {
        // Generic server fail.
        res.sendStatus(500);
    });
});

/* Saving all texts as one JSON string.  The expected body for
   the PUT is a top-level array of{ text: ..., date: ... } */
router.put("/put", function(req, res, next) /* verified */ {
  // Sad path.  Not authorized.
  if (!req.session.isLoggedIn) {
    res.sendStatus(403);
    return;
  }

  /* Main / happy path, authorized.
   * Texts file set to input JSON. */

  let json = req.body;

  lazyInitService()
    .then(() => service.putAllJson(json))
    .then((texts) => {
      if (texts === null) {
        throw new Error();
      }

      res.send(texts);
    })
    .catch(() => {
      // Generic server fail.
      res.sendStatus(500);
    });
});


module.exports = router;

