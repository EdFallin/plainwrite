/**/

import fs from "fs";
import fsp from "fs/promises";
import path from "path";

export class TextService {
  // region Private fields

  // Path is relative to the app, not this class.
  #textPath = "./private/texts";
  #textFile = "texts.json";
  #textFull;

  // endregion Private fields

  constructor() {
    this.#ensurePathExists();
    this.#calculateFull();
    this.#ensureFileExists();
  }

  // region Dependencies of constructor()

  #ensurePathExists() {
    let doesExist;

    try {
      fs.accessSync(this.#textPath, fs.F_OK);
      doesExist = true;
    }
    catch (thrown) {
      doesExist = false;
    }

    if (!doesExist) {
      fs.mkdirSync(this.#textPath);
    }
  }

  #calculateFull() {
    let full = path.join(this.#textPath, this.#textFile);
    this.#textFull = full;
  }

  #ensureFileExists() {
    if (!fs.existsSync(this.#textFull)) {
      fs.writeFileSync(this.#textFull, JSON.stringify([]));
    }
  }

  // endregion Dependencies of constructor()

  async getAllTexts() /* verified */ {
    try {
      let content = await fsp.readFile(this.#textFull, "utf-8");
      let texts = JSON.parse(content);
      return texts;
    }
    catch {
      return null;
    }
  }

  async saveNewText(storeable) /* verified */ {
    try {
      // Get existing as an array.
      let texts = await this.getAllTexts();

      // Place new text at the beginning.
      texts.unshift(storeable);

      // Convert to saveable string.
      let saveable = JSON.stringify(texts);

      // Save and return the result, or a fail value.
      await fsp.writeFile(this.#textFull, saveable);
      return texts;
    }
    catch {
      return null;
    }
  }

  async getAllJson() /* verified */ {
    try {
      let content = await fsp.readFile(this.#textFull, "utf-8");
      return content;
    }
    catch {
      return null;
    }
  }

  async putAllJson(json) /* verified */ {
    try {
      let jsonText = JSON.stringify(json);
      await fsp.writeFile(this.#textFull, jsonText);

      // Reducing round-trips.
      let texts = await this.getAllTexts();
      return texts;
    }
    catch {
      return null;
    }
  }
}
