/**/

import fsp from "fs/promises";
import path from "path";
import bcrypt from "bcrypt";

export class UserService {
  // Path is relative to the app, not this class.
  #userPath = "./private/users";
  #userFile = "users.json";
  #userFull;

  constructor() {
    /* Folder and file are assumed to exist. */
    this.#userFull = path.join(this.#userPath, this.#userFile);
  }

  async doMatch(individual, secret) {
    // Getting any such user from a file.
    let user = await this.#retrieveUser(individual);

    // If no such user, secrets don't match.
    if (!user) {
      return false;
    }

    // Hashes compared.
    let secretsDoMatch = await bcrypt.compare(secret, user.secret);
    return secretsDoMatch;
  }

  async #retrieveUser(individual) {
    // Reading the file's JSON.
    let source = await fsp.readFile(this.#userFull);
    let json = JSON.parse(source);

    // Finding the user, if any.
    let user = json
      .find(x => x.individual === individual);

    // May be undefined.
    return user;
  }
}
