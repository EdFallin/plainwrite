/**/

let express = require("express");
let path = require("path");
let cookieParser = require("cookie-parser");
let session = require("express-session");

let VolatileStore = require("memorystore")(session);

let usersRouter = require("./routes/users");
let textsRouter = require("./routes/texts");

let logger = require("morgan");

let app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

let sessionKey = "EcrHxW8o32SjUGXTIy95gOMMxaLj18Zu8IP6ceSRuIb3nBfRfaFP9GPE2cgvON/73dx5b9Qa7uOewD1jEK";
let sessionOptions = {
  secret: sessionKey,
  cookie: {
    maxAge: 86400000,  // 24 hours.
    sameSite: true
  },
  store: new VolatileStore({ checkPeriod: 86400000 }),  // Recycles every 24 hours.
  rolling: true,
  saveUninitialized: false,
  resave: false,
}

app.use(session(sessionOptions));

app.use(express.static(path.join(__dirname, "public")));

app.options

app.use("/users", usersRouter);
app.use("/texts", textsRouter);

module.exports = app;
