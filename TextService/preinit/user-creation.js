/**/

let bcrypt = require("bcrypt");
let fs = require("fs");

let saltRounds = 10;
let usersJsonPath = "./TextService/private/users/users.json";

// region Functions to run in script

async function hashAndWrite() {
  console.log(`Hashing script started.`);

  let secret = fromArgsToSecret();

  if (!secret) {
    console.log("No secret provided as --secret=_secret_.  No hashing performed.");
    return;
  }

  let hashed = await bcrypt.hash(secret, saltRounds);
  console.log(`"${ secret }" hashed to: "${ hashed }" and saved for the user.`);

  writeHashToJson(hashed);

}

function fromArgsToSecret() {
  /* Secret may be npm arg or node arg, so both
     are tried.  Various parsing cases are handled. */

  let secret = fromNpmArgToSecret() || fromNodeArgToSecret();
  return secret;
}

function fromNpmArgToSecret() {
  let secret = process.env.npm_config_secret;

  // When npm arg is just `--secret`.
  if (secret === "true") {
    return null;
  }

  return secret;
}

function fromNodeArgToSecret() {
  let args = process.argv;
  let secret = args.find(x => x.startsWith("--secret"));

  // When there is no node arg.
  if (!secret) {
    return null;
  }

  // When node arg is just `--secret`.
  if (secret === "--secret") {
    return null;
  }

  secret = secret.replace("--secret=", "");
  return secret;
}

function writeHashToJson(hash) {
  // Target file assumed to exist, relative to root folder.
  let asText = fs.readFileSync(usersJsonPath, "utf8");
  let json = JSON.parse(asText);

  let target = json[0];
  target.secret = hash;

  asText = JSON.stringify(json);
  fs.writeFileSync(usersJsonPath, asText, "utf8");
}

// endregion Functions to run in script

// region The script: Just running the hashing function

hashAndWrite()
  .then(() => {
    console.log(`Hashing script finished.`);
  });

// endregion The script: Just running the hashing function

