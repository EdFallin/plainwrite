/**/

import { ComponentFixture, TestBed } from "@angular/core/testing";

import { PastTextsComponent } from "./past-texts.component";

describe("PastTextsComponent", () => {
  let component: PastTextsComponent;
  let fixture: ComponentFixture<PastTextsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PastTextsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PastTextsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
