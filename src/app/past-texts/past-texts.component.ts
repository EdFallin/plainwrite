/**/

import { Component, OnInit } from "@angular/core";
import { StoredText } from "../stored-text";
import { TextSupplierService } from "../text-supplier/text-supplier.service";
import { DisplayStateService } from "../display-state/display-state.service";

@Component({
  selector: "past-texts",
  templateUrl: "./past-texts.component.html",
  styleUrls: [ "./past-texts.component.css" ]
})
export class PastTextsComponent implements OnInit {
  pastTexts: StoredText[] = [];

  doDisplay: boolean = true;
  showHideText: string = "Hide All";

  canExpand: boolean = false;
  canCollapse: boolean = true;


  constructor(private textSupplier: TextSupplierService, private state: DisplayStateService) {
    // Initial load of stored texts.
    this.textSupplier.retrieveTexts()
      .subscribe((next) => {
        this.pastTexts = next;
      });

    this.textSupplier.textWasAdded
      .subscribe(
        // JS-style bind() needed here because
        // TS still has set vs. call syntax (!).
        this.whenTextIsAdded.bind(this)
      );

    this.state.allAreExpandedWasChanged
      .subscribe(
        // JS-style bind() still needed for set syntax (!).
        this.whenAllAreExpandedIsChanged.bind(this)
      );
  }

  ngOnInit(): void {
    /* No operations. */
  }

  whenShowHideClicked() {
    this.doDisplay = !this.doDisplay;

    if (this.doDisplay) {
      this.showHideText = "Hide All";
    }
    else {
      this.showHideText = "Show All";
    }
  }

  whenExpandClicked() {
    this.canExpand = false;
    this.canCollapse = true;

    this.state.setDoExpandAll(true);
  }

  whenCollapseClicked() {
    this.canExpand = true;
    this.canCollapse = false;

    this.state.setDoExpandAll(false);
  }

  whenTextIsAdded() {
    this.textSupplier.texts
      .subscribe((next) => this.pastTexts = next);
  }

  whenAllAreExpandedIsChanged() {
    this.canExpand = this.state.anyAreCollapsed;
    this.canCollapse = this.state.anyAreExpanded;
  }
}
