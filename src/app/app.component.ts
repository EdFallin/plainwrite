/**/

import { Component } from "@angular/core";
import { SecuringService } from "./securing/securing.service";
import { Observable } from "rxjs";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: [ "./app.component.css" ]
})
export class AppComponent {
  title = "PlainWrite";

  isLoggedIn: boolean;

  constructor(private securing: SecuringService) {
    this.securing.isLoggedIn()
      .then((next) => {
        this.isLoggedIn = next;
      });
  }

  /* Responds to successful login
     made through LoginComponent. */
  whenDidLogIn(): void {
    this.isLoggedIn = true;
  }

  whenLogOutButtonClicked(): void {
    this.securing.logOut()
      .then((next) => this.isLoggedIn = next);
  }
}
