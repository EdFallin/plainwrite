/**/

export type StoredText = {
  text: string;
  datetime: Date;
};

