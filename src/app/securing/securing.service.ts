/**/

import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { SecuringState } from "../securing-state";

@Injectable({
  providedIn: "root"
})
export class SecuringService {
  constructor(private http: HttpClient) {
    /* No operations. */
  }

  async isLoggedIn(): Promise<boolean> {
    // Finding authorization state at the server.
    let state = await this.http
      .get<SecuringState>("/users/condition")
      .toPromise();

    return state.isLoggedIn;
  }

  async tryLogIn(individual: string, secret: string): Promise<boolean> {
    // Authenticating and authorizing at the server.
    let state = await this.http
      .post<SecuringState>("/users/login", { individual, secret })
      .toPromise();

    return state.isLoggedIn;
  }

  async logOut(): Promise<boolean> {
    // De-authorizing at the server.
    let state = await this.http
      .post<SecuringState>("/users/logout", {})
      .toPromise();

    return state.isLoggedIn;
  }
}
