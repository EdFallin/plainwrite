import { TestBed } from '@angular/core/testing';

import { SecuringService } from './securing.service';

describe('SecuringService', () => {
  let service: SecuringService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SecuringService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
