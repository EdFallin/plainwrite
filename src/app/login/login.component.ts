/**/

import { Component, OnInit } from "@angular/core";
import { Output, EventEmitter } from "@angular/core";
import { SecuringService } from "../securing/securing.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: [ "./login.component.css" ]
})
export class LoginComponent implements OnInit {
  @Output() didLogIn: EventEmitter<void>;

  individual: string;
  secret: string;

  constructor(private securing: SecuringService) {
    this.didLogIn = new EventEmitter<void>();
  }

  ngOnInit(): void {
    // Plain JS to set focus.
    let individualInput = document.getElementById("Individual");
    individualInput.focus();
  }

  whenKeyUp(e: KeyboardEvent) {
    if (e.key === "Enter") {
      this.tryLogIn();
    }
  }

  whenBeginButtonClicked() {
    this.tryLogIn();
  }

  tryLogIn() {
    /* The service handles authorization with the server. */
    let didLogIn = this.securing
      .tryLogIn(this.individual, this.secret)
      .then((next) => {
        // Readability.
        let isLoggedIn = next;

        // Not logged in.
        if (!isLoggedIn) {
          return;
        }

        // Is logged in.
        this.didLogIn.emit();
      });
  }
}
