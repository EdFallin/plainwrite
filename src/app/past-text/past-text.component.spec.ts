/**/

import { ComponentFixture, TestBed } from "@angular/core/testing";

import { PastTextComponent } from "./past-text.component";

describe("PastTextComponent", () => {
  let component: PastTextComponent;
  let fixture: ComponentFixture<PastTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PastTextComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PastTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
