/**/

import { Component, OnInit } from "@angular/core";
import { Input } from "@angular/core";
import { DisplayStateService } from "../display-state/display-state.service";
import { Output, EventEmitter } from "@angular/core";

@Component({
  selector: "past-text",
  templateUrl: "./past-text.component.html",
  styleUrls: [ "./past-text.component.css" ]
})
export class PastTextComponent implements OnInit {
  @Input() text: string;
  @Input() datetime: Date;

  doDisplay: boolean = true;
  showHideText: string = "–";

  get textDate(): string {
    let date = new Date(this.datetime);
    return date.toLocaleDateString();
  }

  get textTime(): string {
    let time = new Date(this.datetime);
    return time.toLocaleTimeString();
  }

  constructor(private state: DisplayStateService) {
    this.state.doExpandAllWasChanged
      .subscribe((next) => {
        this.setShowHideState(next);
      });
  }

  ngOnInit(): void {
    this.state.pastTexts.push(this);
  }

  whenShowHideClicked() {
    this.doDisplay = !this.doDisplay;
    this.setShowHideButtonState();
    this.state.raiseAllAreExpandedWasChanged();
  }

  setShowHideState(doDisplay: boolean) {
    this.doDisplay = doDisplay;
    this.setShowHideButtonState();
  }

  setShowHideButtonState() {
    if (this.doDisplay) {
      this.showHideText = "–";
    }
    else {
      this.showHideText = "+";
    }
  }

  supplyButtonClass() {
    if (this.doDisplay) {
      return "";
    }
    else {
      return "reversed";
    }
  }
}
