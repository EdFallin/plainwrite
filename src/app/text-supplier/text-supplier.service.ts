/**/

import { Injectable } from "@angular/core";
import { Output, EventEmitter } from "@angular/core";
import { StoredText } from "../stored-text";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { of as ObservableOf } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class TextSupplierService {
  /* &cruft, perhaps treat 404s or similar coming across */

  // region Fields

  private storedTexts: Observable<StoredText[]>;

  // endregion Fields

  // region Properties

  get texts(): Observable<StoredText[]> {
    return this.storedTexts;
  }

  // endregion Properties

  // region Eventing

  @Output() textWasAdded: EventEmitter<StoredText[]>;

  // endregion Eventing

  constructor(public http: HttpClient) {
    this.textWasAdded = new EventEmitter<StoredText[]>();
  }

  // region Key methods

  addNewText(text: StoredText) {
    // Returned value is unwrapped, so it has to be re-wrapped.
    this.http.post<StoredText[]>("/texts/post", text)
      .subscribe(next => {
        this.storedTexts = ObservableOf(next);
        this.textWasAdded.emit(next);
      });
  }

  retrieveTexts(): Observable<StoredText[]> {
    this.storedTexts = this.http.get<StoredText[]>("/texts/get");
    return this.storedTexts;
  }

  // endregion Key methods
}
