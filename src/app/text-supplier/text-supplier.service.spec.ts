/**/

import { TestBed } from "@angular/core/testing";

import { TextSupplierService } from "./text-supplier.service";

describe("TextSupplierService", () => {
  let service: TextSupplierService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TextSupplierService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
