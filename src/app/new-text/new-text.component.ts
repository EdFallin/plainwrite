/**/

import { Component, OnInit } from "@angular/core";
import { StoredText } from "../stored-text";
import { TextSupplierService } from "../text-supplier/text-supplier.service";

@Component({
  selector: "new-text",
  templateUrl: "./new-text.component.html",
  styleUrls: [ "./new-text.component.css" ]
})
export class NewTextComponent implements OnInit {
  text: string = "";

  constructor(private textSupplier: TextSupplierService) {
    /* No operations. */
  }

  ngOnInit(): void {
    // Plain JS to set focus.
    let newTextArea = document.getElementById("NewTextArea");
    newTextArea.focus();
  }

  whenKeyDown(e: KeyboardEvent) {
    if ((e.ctrlKey || e.metaKey) && e.key === "Enter") {
      this.saveNewText();
    }
  }

  whenSaveClicked() {
    this.saveNewText();
  }

  saveNewText() {
    // These are stored together.
    let storedText = { text: this.text, datetime: new Date() };

    // Actually saving.
    this.textSupplier.addNewText(storedText);

    // Clearing the value now saved.
    this.text = "";
  }
}
