/**/

import { Injectable } from '@angular/core';
import { Output, EventEmitter } from "@angular/core";
import { PastTextComponent } from "../past-text/past-text.component";

@Injectable({
  providedIn: 'root'
})
export class DisplayStateService {
  pastTexts: PastTextComponent[] = [];

  doExpandAll: boolean;
  doCollapseAll: boolean;

  @Output() doExpandAllWasChanged: EventEmitter<boolean>;
  @Output() allAreExpandedWasChanged: EventEmitter<void>;


  constructor() {
    this.doExpandAll = true;
    this.doCollapseAll = false;

    this.doExpandAllWasChanged = new EventEmitter<boolean>();
    this.allAreExpandedWasChanged = new EventEmitter<void>();
  }

  setDoExpandAll(doExpand: boolean): void {
    this.doExpandAll = doExpand;
    this.doExpandAllWasChanged.emit(this.doExpandAll);
  }

  raiseAllAreExpandedWasChanged() {
    this.allAreExpandedWasChanged.emit();
  }

  get anyAreExpanded(): boolean {
    let anyAre = this.pastTexts
      .some(x => x.doDisplay);

    return anyAre;
  }

  get anyAreCollapsed(): boolean {
    let anyAre = this.pastTexts
      .some(x => !x.doDisplay);

    return anyAre;
  }
}
