/**/

import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { NewTextComponent } from "./new-text/new-text.component";
import { PastTextComponent } from "./past-text/past-text.component";
import { PastTextsComponent } from "./past-texts/past-texts.component";

import { TextSupplierService } from "./text-supplier/text-supplier.service";
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    NewTextComponent,
    PastTextComponent,
    PastTextsComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ TextSupplierService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
